package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

type XDCTestSuite struct {
	suite.Suite

	userData *UserRegisterData // user data
	userCtx  context.Context   // user login context.
	opsCtx   context.Context   // admin/OPs login context.

	xdcId string // xdc name
}

func TestXDCAPI(t *testing.T) {
	suite.Run(t, &XDCTestSuite{})
}

func (ts *XDCTestSuite) SetupSuite() {

	// We make sure there is an active user and they are logged in.
	ts.userData = murphUser
	ts.xdcId = "myxdc"

	var err error
	ts.opsCtx, err = LoginContext(opsId, opsPw, ts.Assertions)
	ts.Nil(err, ErrorStr(err))

	err = AddActiveUser(ts.userData, ts.opsCtx, ts.Assertions)
	ts.Nil(err, ErrorStr(err))

	// Get login context for user
	ts.userCtx, err = LoginContext(ts.userData.Username, ts.userData.Password, ts.Assertions)
	ts.Require().Nil(err, ErrorStr(err))
}

func (ts *XDCTestSuite) TearDownSuite() {

	err := DeleteActiveUser(ts.userData.Username, ts.opsCtx, ts.Assertions)
	ts.Require().Nil(err, ErrorStr(err))

	// err = DeleteXDC(ts.userCtx, ts.userData.Username, ts.xdcId)
	// ts.Require().Nil(err, ErrorStr(err))
}

func (ts *XDCTestSuite) TestCreateXDC() {

	ts.Equal(1, 1)
	err := CreateXDC(
		ts.userCtx,
		ts.userData.Username, // create in the user's personal project
		ts.xdcId,
		portal.XDCType_personal,
	)
	ts.Require().Nil(err, ErrorStr(err))

	// Now confirm it was created
	xdc, status, err := GetXDC(ts.userCtx, ts.userData.Username, ts.xdcId)
	ts.Require().Nil(err, ErrorStr(err))

	// xdc name is fully qualified, i,e, xdcid.projectid, eg "myxdc.murphy"
	ts.Equal(ts.xdcId+"."+ts.userData.Username, xdc.Name)
	ts.Equal(portal.XDCType_personal, xdc.Type)

	// Confirm the portal thinks the XDC is good to go.
	ts.Equal(portal.TaskStatus_Success, status.HighestStatus)
}
