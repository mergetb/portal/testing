package main

// TestCreateProject
// First create a user then add a project for that user.
// Confirm the project is there.
// func TestCreateProject(t *testing.T) {
//
// 	a := assert.New(t)
//
// 	u := &portal.User{
// 		Username:    "olive",
// 		Email:       "olive@example.net",
// 		Name:        "Olive the Cat",
// 		Institution: "The Place",
// 		Category:    "Cat",
// 		Country:     "Canada",
// 	}
// 	pw := "092jlKDAL322qd@@#@#!!"
// 	pid := "oliveproject"
// 	desc := "This is Olive's project"
//
// 	err := AddActiveUser(u, pw, a)
// 	a.Nilf(err, "AddActiveUser error: %v", err)
// 	defer DeleteActiveUser(u.Username, a)
//
// 	ctx := LoginContext(u.Username, pw, a)
//
// 	err = CreateProject(ctx, u.Username, pid, desc)
// 	a.Nilf(err, "CreateProject error: %v", err)
//
// }
