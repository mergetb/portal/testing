Running tests on a minikube-portal.

This directory contains tests that run against a Merge API. To do this, the tests call the Merge API directly using the 
go language bindings. These tests are built into binaries. Then these binaries are run in a container that is in the 
same kubernetes namespace as the portal components. (You can see the portal services by running `kubectl -n merge get pods`.)

How to run the tests:

1. Make sure the "pops" ("Portal OPeratorS") container is running. This is the container from which the tests are run. So it
needs to exist of course. 
    * `kubectl -n merge get pods | grep pops` will show you if it is running
    * if it is not running, run `make pops_ctr`
    * then confirm that it is running.

2. Run the tests via `make run`
    `make run` does a few things. It builds the test binaries if they do not exist. Then it copies the binaries 
    to the pops container, then runs a script that invokes all the tests. 

You should see all tests PASS. Here is a smaple run
```
glawler@meta:~/src/mergetb/portal/testing/test$ make run
kubectl -n merge cp ./build pops:/tmp
kubectl -n merge exec -t pod/pops -- sh -c 'mv /tmp/build/* /usr/local/bin'
kubectl -n merge exec -it pops -- /usr/local/bin/run-tests.sh
INFO[0000] Read OPSID value portalops
INFO[0000] Read OPSPW value NTM2YjhlYWEzYjM0MzljMDgzMDllYTdi
PASS
INFO[0000] Read OPSID value portalops
INFO[0000] Read OPSPW value NTM2YjhlYWEzYjM0MzljMDgzMDllYTdi
INFO[0000] login resp: token:"ory_st_S38TS9CfcDltVZEWQ2LpHJxkR293f1R6"
INFO[0000] init user resp:
INFO[0000] get user resp: user:{username:"murphy"  uid:1000  gid:1000  projects:{key:"murphy"  value:{role:Creator  state:Active}}  ver:1}
INFO[0000] get user state: NotSet
INFO[0000] Deleted 59 keys from etcd
PASS
glawler@meta:~/src/mergetb/portal/testing/test$
```

To add new tests edit the Makefile and:
* add your test binary to the $TESTS variable
* add a stanza that builds your tests. (a `go test -c ...` line)
* add your test binary to the `run_tests.sh` script
  * if you update `run_tests.sh` then restart the pops container
    * `make clean` 
    * `make pops_ctr` 



