package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

//
// Tests of Identity API
//

// TestRegisterUser - tests register
func TestRegisterUser(t *testing.T) {

	a := assert.New(t)

	Register(uid, email, pw, affiliation, position, name, a)
	defer Unregister(uid, a)

	// confirm the new ID is there.
	err := Identity(func(cli portal.IdentityClient) error {
		id, err := cli.GetIdentity(
			context.TODO(),
			&portal.GetIdentityRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "Identity.GetIdentity: %v", err)
		a.Equal(id.Identity.Username, uid)

		t.Logf("ID: %v", id.Identity)

		return nil
	})

	a.Nilf(err, "identity get id: %v", err)
}

// TestRegisterUser - tests register
func TestUnregisterUser(t *testing.T) {

	a := assert.New(t)

	Register(uid, email, pw, affiliation, position, name, a)
	Unregister(uid, a)

	// confirm the ID is not there. We expect non nil error
	Identity(func(cli portal.IdentityClient) error {
		_, err := cli.GetIdentity(
			context.TODO(),
			&portal.GetIdentityRequest{
				Username: uid,
			},
		)

		t.Logf("Get ID error: %v", err)
		a.NotNilf(err, "Identity.GetIdentity: %v", err)

		return nil
	})
}
