package main

import (
	"context"
	"os"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var (
	opsId string
	opsPw string
)

func TestMain(m *testing.M) {
	InitFromEnv()
	os.Exit(m.Run())
}

func InitFromEnv() {

	value, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %s. ignoring", value)
		} else {
			log.Infof("setting log level to %s", value)
			log.SetLevel(lvl)
		}
	}

	vals := map[string]*string{
		"OPSID": &opsId,
		"OPSPW": &opsPw,
	}

	for env, val := range vals {
		v, ok := os.LookupEnv(env)
		if !ok {
			log.Fatalf("Missing required environment variable %s", env)
		}
		log.Infof("Read %s value %s", env, v)
		*val = v
	}
}

func CleanDB() error {

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"etcd:2379"},
		DialTimeout: 2 * time.Second,
	})
	if err != nil {
		return err
	}
	defer cli.Close()

	kvc := clientv3.NewKV(cli)
	resp, err := kvc.Delete(context.TODO(), "/", clientv3.WithPrefix())
	log.Infof("Deleted %d keys from etcd", resp.Deleted)

	// TODO: delete all minio data as well.

	return nil
}
