package main

import (
	"fmt"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	// services - these are the k8s service names as these tests are
	// run within the portal k8s namespace
	idService        = "identity"
	idPort           = "6000"
	workspaceService = "apiserver"
	workspacePort    = "6000"

	// GRPC config.
	GRPCMaxMessageSize = 512 * 1024 * 1024
	GRPCMaxMessage     = grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxCallSendMsgSize(GRPCMaxMessageSize),
	)
)

func connection(svc, port string) (*grpc.ClientConn, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", svc, port),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		GRPCMaxMessage,
	)
	if err != nil {
		return nil, fmt.Errorf("bad connection to %s", svc)
	}

	return conn, nil
}

func Workspace(f func(portal.WorkspaceClient) error) error {

	conn, err := connection(workspaceService, workspacePort)
	if err != nil {
		return err
	}
	defer conn.Close()
	return f(portal.NewWorkspaceClient(conn))
}

func Identity(f func(portal.IdentityClient) error) error {

	conn, err := connection(idService, idPort)
	if err != nil {
		return err
	}
	if err != nil {
		return fmt.Errorf("grpc dial: %v", err)
	}
	defer conn.Close()

	cli := portal.NewIdentityClient(conn)

	return f(cli)
}
