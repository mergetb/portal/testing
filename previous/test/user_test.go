package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func TestInitUser(t *testing.T) {

	defer CleanDB()
	a := assert.New(t)

	Register(uid, email, pw, affiliation, position, name, a)
	defer Unregister(uid, a)

	c := LoginContext(opsId, opsPw, a)
	InitUser(c, uid, a)

	user, _ := GetUser(c, uid, a)
	a.Equal(user.State, portal.UserState_NotSet)
}
