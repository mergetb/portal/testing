package main

import (
	"context"

	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

//
// API calls - these will fail on error, so no need to check the
// error.
//

// Unregister - unregisters the given user.
func Unregister(uid string, a *assert.Assertions) error {

	err := Identity(func(cli portal.IdentityClient) error {
		_, err := cli.Unregister(
			context.TODO(),
			&portal.UnregisterRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "IdentityClient.Register: %v", err)

		return nil
	})

	a.Nilf(err, "identity unregister: %v", err)
	return err
}

// Register - registers the given user.
func Register(uid, email, pw, aff, pos, name string, a *assert.Assertions) error {

	err := Identity(func(cli portal.IdentityClient) error {
		_, err := cli.Register(
			context.TODO(),
			&portal.RegisterRequest{
				Username: uid,
				Email:    email,
				Password: pw,
				Traits: map[string]string{
					"affiliation": aff,
					"position":    pos,
					"name":        name,
				},
			},
		)

		a.Nilf(err, "IdentityClient.Register: %v", err)

		return nil
	})

	a.Nilf(err, "identity connect: %v", err)
	return nil
}
