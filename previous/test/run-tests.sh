#!/usr/bin/env bash

set -e

# get dir the script is in.
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

$SCRIPT_DIR/identity-test
$SCRIPT_DIR/user-test
