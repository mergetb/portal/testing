package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/metadata"
)

func FreezeUser(ctx context.Context, uid string, a *assert.Assertions) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.FreezeUser(
			ctx,
			&portal.FreezeUserRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "WorkspaceClient.FreezeUser: %v", err)
		log.Infof("freeze user resp: %v", resp)
		return nil
	})

	a.Nilf(err, "init user %v", err)

	return err
}

func InitUser(ctx context.Context, uid string, a *assert.Assertions) func() {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.InitUser(
			ctx,
			&portal.InitUserRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "WorkspaceClient.InitUser: %v", err)
		log.Infof("init user resp: %v", resp)
		return nil
	})

	a.Nilf(err, "init user %v", err)

	return func() { FreezeUser(ctx, uid, a) }
}

func GetUser(ctx context.Context, uid string, a *assert.Assertions) (*portal.User, error) {

	var user *portal.User

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.GetUser(
			ctx,
			&portal.GetUserRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "WorkspaceClient.Getuser: %v", err)
		log.Infof("get user resp: %+v", resp)
		log.Infof("get user state: %v", resp.User.State)
		user = resp.User
		return nil
	})

	a.Nilf(err, "get user %v", err)
	return user, nil
}

// Login the user with the given name and password. Return a context that
// can be passed into Merge API calls. The calls will run as if called
// by the user.
func LoginContext(uid, pw string, a *assert.Assertions) context.Context {

	var token string
	err := Identity(func(cli portal.IdentityClient) error {
		resp, err := cli.Login(
			context.TODO(),
			&portal.LoginRequest{
				Username: uid,
				Password: pw,
			},
		)

		a.Nilf(err, "WorkspaceClient.Login: %v", err)
		log.Infof("login resp: %v", resp)
		token = resp.Token

		return nil
	})

	a.Nilf(err, "login %v", err)

	// add the token to the context.
	ctx := context.TODO()
	ctx = metadata.AppendToOutgoingContext(ctx, "Authorization", fmt.Sprintf("Bearer %s", token))

	return ctx
}

func Logout(uid string, a *assert.Assertions) {

	err := Identity(func(cli portal.IdentityClient) error {
		_, err := cli.Logout(
			context.TODO(),
			&portal.LogoutRequest{
				Username: uid,
			},
		)

		a.Nilf(err, "IdentityClient.Logout: %v", err)
		return nil
	})

	a.Nilf(err, "logout: %v", err)
	return
}

func DeleteUser(ctx context.Context, uid string, a *assert.Assertions) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.DeleteUser(
			ctx,
			&portal.DeleteUserRequest{
				User: uid,
			},
		)

		a.Nilf(err, "WorkspaceClient.DeleteUser: %v", err)
		return nil
	})

	a.Nilf(err, "delete user %v", err)
	return nil
}
