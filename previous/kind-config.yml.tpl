kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4

# Setup for reading ffrom a local registry. 
# see: https://kind.sigs.k8s.io/docs/user/local-registry/
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry]
    config_path = "/etc/containerd/certs.d"

#
# nodes - one control plane, one service worker, one xdc worker.
#
# We map a host path to the default kind dynamic provisioner. This
# allows us to have the portal data on the host. By default, 
# dynamic provisioning is at /var/local-path-provisioner on 
# each node. So we map that to a host dir. This may not be needed.
# 
# We also map the well known /var/vol/host to a test specific
# directory. This directory holds the shared PV storage used
# in mergefs and other RWX pvs needed for a merge portal. 
#
nodes:
- role: control-plane
- role: worker         # portal services
  labels:
    mergeworker: services
  extraMounts:
  - hostPath: /mnt/kind/$TESTID
    containerPath: /var/vol/host
  - hostPath: /mnt/kind/$TESTID/storage
    containerPath: /var/local-path-provisioner
- role: worker         # xdc worker
  labels:
    mergeworker: xdc
  extraMounts:
  - hostPath: /mnt/kind/$TESTID
    containerPath: /var/vol/host
  - hostPath: /mnt/kind/$TESTID/storage
    containerPath: /var/local-path-provisioner
  kubeadmConfigPatches:
  - |
    kind: JoinConfiguration
    nodeRegistration:
      kubeletExtraArgs: 
        register-with-taints: "dedicated=xdc_worker:NoSchedule"
