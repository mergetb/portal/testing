#!/usr/bin/env bash

set -e

# This was ansible but the ansible was stupid. So now it's a bash script.

TESTID=$1
DIR=$2
REG=localhost:5000

SVCS="apiserver.tar cred.tar identity.tar apiserver.tar model.tar realize.tar wgsvc.tar xdc.tar materialize.tar ssh-jump.tar commission.tar git-server.tar xdc-base.tar"

# confirm portal containers are there.
for s in $SVCS; do 
    if [[ ! -e $DIR/$s ]]; then 
        echo Build for portal service $DIR/$s is missing. Unable to continue.
        exit 1
    fi
done

# load portal tar images, tag them, then push to REG should be localhost for local kind registry
for s in $SVCS; do 
    echo Loading image $DIR/$s
    image=$(podman image load -i $DIR/$s 2>/dev/null | awk '{ print $3 }')
    path=$(echo $image | cut -d/ -f2-)  # trim registry
    podman tag $image $REG/$path
    podman push --tls-verify=false $REG/$path
done
