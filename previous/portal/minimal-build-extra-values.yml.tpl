image:
  repository: "localhost:5000/mergetb/portal/services"
  pullPolicy: Always
  tag: $TESTID

certIssuer: true

opsEmail: ops@example.net
opsId: portalops
opsPw: NTFmYzk2ZTM0YWQ5ZTQ4NjJhYmZkZTkx

apiserver:
  ingress:
    enabled: false

cred:
  certProvisioner: ops@mergetb.net

gitserver:
  ingress:
    enabled: false
  persistence:
    storageClass: manual


sshjump:
  fqdn: jump.footest.example.net

xdc:
  hostAliases: "192.168.126.10,grpc.$TESTID.example.net,api.$TESTID.example.net,git.$TESTID.example.net"
  xdcDomain: xdc.$TESTID.example.net

etcd:
  persistence:
    enabled: false
  image:
    registry:
      localhost:5000

step-certificates:
  releaseName: stepca
  ca:
    db:
      persistent: false

wgd:
  disabled: true

minio:
  auth:
    rootPassword: Y2Y0MDAwZWUzZTU4ODI2NjI2NWY4ZWQ0
  persistence:
    enabled: false

mergefs:
  persistence:
    storageClass: manual

sshJump:
  disabled: true
  repository_path: merge/xdc/ssh-jump
  persistence:
    storageClass: manual
