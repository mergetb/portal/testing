#!/usr/bin/env bash

#
# TODO: test for pre reqs:
#  * kind - kubernetes in docker
#  * helm - install things in k8s
#  * cmctl - validate cert-mananger in k8s
#    install on linux:
#       OS=$(go env GOOS); ARCH=$(go env GOARCH); 
#       curl -fsSL -o cmctl.tar.gz https://github.com/cert-manager/cert-manager/releases/latest/download/cmctl-$OS-$ARCH.tar.gz
#       tar xzf cmctl.tar.gz
#       sudo mv cmctl /usr/local/bin
#

trap 'cleanup' ERR SIGINT

function cleanup {
    # kind delete cluster --name $TESTID
    # rm -rf $DATADIR
    # rm -f $KINDCFG
    # rm -f nginx-ingress-deploy-$TESTID.yml
    # # TODO: delete tag/image from local registry
    exit 1
}

function randid {
    date +%N | sha256sum | base64 | head -c 8 | tr '[A-Z]' '[a-z]'; echo
}

FORMAT=${FORMAT:-CI}

# unique ID per test run.
export TESTID=${TESTID:-test-$(randid)}
export INGPORT80=$((8080 + RANDOM % 1024))
export INGPORT443=$((8080 + RANDOM % 1024))
export NODEHOST=localhost

export PORTALBUILD=../services/build

echo
echo Building configuration for this test run.
KINDCFG=kind-config-$TESTID.yml
cat kind-config.yml.tpl | envsubst > $KINDCFG
cat nginx-ingress-deploy.yml.tpl | envsubst > nginx-ingress-deploy-$TESTID.yml
cat portal/minimal-build-extra-values.yml.tpl | envsubst > portal/minimal-build-extra-values.yml

DATADIR=/mnt/kind/$TESTID
mkdir -p $DATADIR
mkdir -p $DATADIR/mergefs
mkdir -p $DATADIR/git
mkdir -p $DATADIR/storage

echo
echo Running kind to start the k8s cluster
kind create cluster --name $TESTID --config $KINDCFG

CTX=kind-$TESTID

# may not want to set this if we're running parallel tests...
kubectl config use-context $CTX

#
# Set default storage type to RWX so mergefs and others PVC will work.
#
kubectl -n local-path-storage patch cm local-path-config --type merge --patch-file local-path-config-patch.yml

# setup the local registry for use in this cluster.
# see https://kind.sigs.k8s.io/docs/user/local-registry/
./setup-kind-local-registry.sh
./load_images.sh $TESTID $PORTALBUILD

if [[ $FORMAT == APPLIANCE ]]; then 

    # install cert manager
    echo 
    echo Installing cert-manager
    helm \
        --kube-context $CTX \
        install cert-manager jetstack/cert-manager --version v1.11.0 \
        --namespace cert-manager \
        --create-namespace \
        --set installCRDs=true
    
    echo
    echo Checking cert-manager installation
    cmctl check api
    
    echo
    echo Installing NGINX Ingress
    # see https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx
    # needed to update this as kind seems to have drifted
    kubectl apply -f nginx-ingress-deploy-$TESTID.yml
    
    echo
    echo Wait for NGINX Ingress
    kubectl wait --namespace ingress-nginx \
        --for=condition=ready pod \
        --selector=app.kubernetes.io/component=controller \
        --timeout=90s

fi # APPLIANCE

echo 
echo Configuring merge ansible
pushd ansible
NAMESPACE=merge
PORTAL_FQDN=mergetb.example.net
cat extravars.yml.tpl | ./generate.sh > extravars.yml
popd

# Now that we've installed the 3rd party things, taint the node so that
# portal containers are deployed to the right "nodes".
# (Note that the kind config, taints the xdc node at the start.)
kubectl taint node -l mergeworker=services dedicated=service_worker:NoSchedule

pushd ansible

# configure static volumes
echo
echo Creating Merge volumes
ansible-playbook -i inventory -e @extravars.yml volumes.yml 

# start installs.
echo
echo Installing Merge Authentication
ansible-playbook -i inventory -e @extravars.yml auth.yml

echo
echo Installing step-ca
ansible-playbook -i inventory -e @extravars.yml stepca.yml

# wait for installs. 
echo
echo Waiting for Auth to be ready
kubectl wait \
    --namespace $NAMESPACE \
    --for=condition=ready pod \
    --selector=app.kubernetes.io/name=kratos \
    --timeout=90s

echo Waiting for step-ca to be ready
kubectl wait \
    --namespace $NAMESPACE \
    --for=condition=ready pod \
    --selector=statefulset.kubernetes.io/pod-name=stepca-step-certificates-0 \
    --timeout=90s

popd # ansible

# TODO: build and push portal containers to local registry tagged with testid. 
# Then update the portal install to point to the local registry.
echo
echo Installing merge portal 
pushd portal
helm install -n $NAMESPACE --values minimal-build-extra-values.yml portal .
popd 

echo 
echo Running Tests
pushd test
make CLUSTER=$TESTID run
popd
