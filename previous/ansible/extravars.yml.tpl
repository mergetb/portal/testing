tolerations:
  svc:
    tolerations:
      - effect: NoSchedule
        key: dedicated
        operator: Equal
        value: service_worker
  wrk:
    tolerations:
      - effect: NoSchedule
        key: dedicated
        operator: Equal
        value: service_worker

# Local host updates (for portal appliance install only)
local:
    resolve: true
    applianceaddr: $HOST_IP

# k8s namespace
ns: $NAMESPACE

#
# Auth settings.
# 
auth:
  dbpass: $AUTHDB_PASS
  postgres_user: $AUTHDB_USER
  postgres_db: $AUTHDB_DB
  kratos_cookie: $AUTH_COOKIE
  kratos_cipher: $AUTH_CIPHER
  appname: "auth"
  registration_webhook: $REGISTER_WEBHOOK
  oidc_methods:
    # uncomment to add support for google and gitlab logins.
    # will need to create own id and secrets for new installations.
    #   config:
    #     providers:
    #       # google
    #       - client_id: 752880253173-29ab6i8e9718ertk3absqdkurk6qupnb.apps.googleusercontent.com
    #         client_secret: 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
    #       # gitlab
    #       - client_id: 8023378cabf1de3c79d7d8cf3bf7d8150edf73ca1a8d9d92be00a8c4c08298ed
    #         client_secret: 966c14a50c240d376171b8f4b802564eae05a863e6fcb1dfc0ca64d64fbb8500


#
# Smallstep CA
#
stepca:
  capw: $STEPCA_PW
  provpw: $STEPPROV_PW
  caname: step-ca
  provisioner: ops@mergetb.net
  appname: stepca

#
# Merge Portal installation specific variables.
#
portal:
  copy_existing: $COPY_EXISTING 
  appname: portal
  fqdn: $PORTAL_FQDN
  repository: $PORTAL_REPO
  pullPolicy: Always
  tag: $PORTAL_TAG
  opsId: $OPSID
  opsEmail: $OPSEMAIL
  opspw: $OPSPW
  miniopw: $MINIOPW
  # This port must be unique across all portal installs in the cluster.
  # default value is 36000
  wgdport: $WGDPORT

#
# Launch
#
launch:
  repository: $LAUNCH_REPO
  pullPolicy: Always
  tag: $LAUNCH_TAG

#
# Information about persistent volumes in the cluster.
#
# (These can be created using volumes.yml if needed.)
# 
pvs:
  mergefs:
    name: "mergefs"
    storage: 10Gi
    accessmode: ReadWriteMany
    path: "mergefs"
    ns: $NAMESPACE
  mergetfsxdc:
    name: "mergefs-xdc"
    storage: 10Gi
    accessmode: ReadWriteMany
    path: "mergefs"
    ns: $NAMESPACE-xdc # by convention. 
  git:
    name: "git-server"
    storage: 10Gi
    accessmode: ReadWriteOnce
    path: "git"
    ns: $NAMESPACE
