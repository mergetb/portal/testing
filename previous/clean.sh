#!/usr/bin/env bash

c=$(kind -q get clusters)
if [[ $c != "" ]]; then
    kind delete clusters $c
fi

rm -f kind-config-test-*.yml
rm -f nginx-ingress-deploy-test-*.yml
rm -rf /mnt/kind/*
rm -f test/pops.yaml
