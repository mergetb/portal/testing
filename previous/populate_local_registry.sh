#!/usr/bin/env bash

set -e 

# Get and push 3rd party conatiners.

for c in \
    docker.io/oryd/kratos:v0.11.1 \
    cr.step.sm/smallstep/step-ca:0.25.0 \
    docker.io/bitnami/etcd:3.5.4-debian-11-r0 \
    ; \
do
    p=$(echo $c | cut -d / -f2-)
    localc=localhost:5000/$p

    podman image exists $c
    if [ $? -ne 0 ]; then 
        podman pull $c
    fi

    podman image exists $localc
    if [ $? -ne 0 ]; then
        podman tag $c $localc
    fi

    echo Pushing $localc to local registry
    podman push --tls-verify=false $localc
done

# to add cert-mananger
# also all the k8s containers?
# postgres (for kratos)
