package main

import (
	"context"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func CreateXDC(ctx context.Context, pid, xdcid string, xdctype portal.XDCType) error {

	err := XDC(func(cli portal.XDCClient) error {
		_, err := cli.CreateXDC(
			ctx,
			&portal.CreateXDCRequest{
				Project: pid,
				Xdc:     xdcid,
				Type:    xdctype,
			},
		)
		return err
	})
	return err
}

func GetXDC(ctx context.Context, pid, xdcid string) (*portal.XDCInfo, *portal.TaskTree, error) {

	var info *portal.XDCInfo
	var status *portal.TaskTree

	err := XDC(func(cli portal.XDCClient) error {
		resp, err := cli.GetXDC(
			ctx,
			&portal.GetXDCRequest{
				Project:  pid,
				Xdc:      xdcid,
				StatusMS: 60000, // wait for a minute
			},
		)
		if resp != nil {
			info = resp.Xdc
			status = resp.Status
		}
		return err
	})

	return info, status, err
}

func DeleteXDC(ctx context.Context, pid, xdcid string) error {

	err := XDC(func(cli portal.XDCClient) error {
		_, err := cli.DeleteXDC(
			ctx,
			&portal.DeleteXDCRequest{
				Project: pid,
				Xdc:     xdcid,
			},
		)
		return err
	})
	return err
}
