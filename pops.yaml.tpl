apiVersion: v1
kind: Pod
metadata:
  namespace: merge
  name: pops
  labels:
    app: pops
spec:
  containers:
  - name: pops
    image: docker.io/mergetb/pops:latest
    imagePullPolicy: Always
    env:
      - name: LOGLEVEL
        value: info
      - name: OPSID
        value: $OPSID
      - name: OPSPW
        value: $OPSPW
