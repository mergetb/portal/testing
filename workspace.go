package main

import (
	"context"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func CreateProject(ctx context.Context, username, project, desc string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.CreateProject(
			ctx,
			&portal.CreateProjectRequest{
				User: username,
				Project: &portal.Project{
					Name:        project,
					Description: desc,
				},
			},
		)

		return err
	})

	return err
}
