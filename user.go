package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func FreezeUser(ctx context.Context, uid string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.FreezeUser(
			ctx,
			&portal.FreezeUserRequest{
				Username: uid,
			},
		)

		log.Debugf("freeze user resp: %v", resp)
		return err
	})

	return err
}

func ActivateUser(ctx context.Context, uid string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.ActivateUser(
			ctx,
			&portal.ActivateUserRequest{
				Username: uid,
			},
		)

		log.Debugf("ActivateUser resp: %v", resp)
		return err
	})

	return err
}

func InitUser(ctx context.Context, uid string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.InitUser(
			ctx,
			&portal.InitUserRequest{
				Username: uid,
			},
		)

		log.Debugf("init user resp: %v", resp)
		return err
	})

	return err
}

func GetUser(ctx context.Context, uid string) (*portal.User, error) {

	var user *portal.User

	err := Workspace(func(cli portal.WorkspaceClient) error {
		resp, err := cli.GetUser(
			ctx,
			&portal.GetUserRequest{
				Username: uid,
			},
		)

		if resp != nil {
			log.Debugf("get user resp: %+v", resp)
			log.Debugf("get user state: %v", resp.User.State)
			user = resp.User
		} else {
			log.Debugf("no resp from GetUser")
		}
		return err
	})

	return user, err
}

func Logout(uid string) error {

	err := Identity(func(cli portal.IdentityClient) error {
		_, err := cli.Logout(
			context.TODO(),
			&portal.LogoutRequest{
				Username: uid,
			},
		)

		return err
	})

	return err
}

func DeleteUser(ctx context.Context, uid string) error {

	err := Workspace(func(cli portal.WorkspaceClient) error {
		_, err := cli.DeleteUser(
			ctx,
			&portal.DeleteUserRequest{
				User: uid,
			},
		)

		return err
	})

	return err
}

// AddActiveUser is a non API call to add an active user to the system.
// Useful for creating a user for further testing.
func AddActiveUser(u *UserRegisterData, opsCtx context.Context, a *assert.Assertions) error {

	err := Register(u)
	if err != nil {
		return err
	}

	err = InitUser(opsCtx, u.Username)
	if err != nil {
		return err
	}

	err = ActivateUser(opsCtx, u.Username)
	if err != nil {
		return err
	}

	return nil
}

func DeleteActiveUser(uid string, opsCtx context.Context, a *assert.Assertions) error {
	err := DeleteUser(opsCtx, uid)
	if err != nil {
		return err
	}
	err = Unregister(opsCtx, uid)
	if err != nil {
		return err
	}
	return nil
}
