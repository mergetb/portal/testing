package main

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

type UserTestSuite struct {
	suite.Suite // struct composition to include all suite deatils.

	opsCtx   context.Context   // login context of ops
	userCtx  context.Context   // login context of user
	userData *UserRegisterData // user data
}

// Invoke thte test suite via a normal go test
func TestUserCrud(t *testing.T) {
	suite.Run(t, &UserTestSuite{})
}

func (uts *UserTestSuite) SetupSuite() {

	uts.userData = murphUser

	var err error
	uts.opsCtx, err = LoginContext(opsId, opsPw, uts.Assertions)
	uts.Nil(err, ErrorStr(err))

	uts.T().Logf("created user: %+v", uts.userData)
}

func (uts *UserTestSuite) BeforeTest(suitName, testName string) {
	uts.T().Logf("Before Test %s", testName)

	var err error
	//can now run all test
	if testName != "TestActivateUser" {

		err = AddActiveUser(uts.userData, uts.opsCtx, uts.Assertions)
		uts.Nil(err, ErrorStr(err))
	} else {
		err := Register(murphUser)
		uts.Nil(err, ErrorStr(err))
	}

	uts.userCtx, err = LoginContext(uts.userData.Username, uts.userData.Password, uts.Assertions)
	uts.Require().Nil(err, ErrorStr(err))
}

func (uts *UserTestSuite) AfterTest(suitName, testName string) {
	uts.T().Logf("After Test %s", testName)
	if testName != "TestDeleteUser" {
		err := DeleteUser(uts.opsCtx, murphUser.Username)
		uts.Nil(err, ErrorStr(err))
	} else {
		Unregister(uts.opsCtx, murphUser.Username)
	}
}

func (uts *UserTestSuite) TearDownSuite() {

	err := DeleteActiveUser(
		uts.userData.Username,
		uts.opsCtx,
		uts.Assertions,
	)
	uts.Nil(err, ErrorStr(err))
}

func (uts *UserTestSuite) TestFreezeUser() {

	//user already created and activated
	err := FreezeUser(
		uts.opsCtx,
		uts.userData.Username,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("Tryed to freez user %s", uts.userData.Username)

	user, err := GetUser(
		uts.opsCtx,
		uts.userData.Username,
	)
	uts.Nil(err, ErrorStr(err))
	uts.NotNil(user, "If you are seeing this user is Nil and should not be")
	uts.Equal(portal.UserState_Frozen, user.State, "User should be in a Frozen state")
	uts.T().Logf("User %s frezze status is : %v", uts.userData.Username, user.State)
}

func (uts *UserTestSuite) TestUserCreate() {

	// the user has been created and is active at this point so just check the data.
	user, err := GetUser(uts.opsCtx, uts.userData.Username)
	uts.Require().Nil(err, ErrorStr(err))

	uts.T().Log("user", user)

	// Confirm the user data is correct
	uts.Equal(portal.UserState_Active, user.State)
	uts.Equal(uts.userData.Username, user.Username)
	uts.Equal(uts.userData.Name, user.Name)
	uts.NotZero(user.Gid)
	uts.NotZero(user.Uid)
	uts.False(user.Admin)
	uts.Empty(user.Facilities)
	uts.Empty(user.Organizations)
	uts.NotEmpty(user.Projects)
	uts.Contains(user.Projects, uts.userData.Username) // personal project exists.
	uts.Len(user.Projects, 1)                          // only one project - the personal project
	uts.Equal(portal.Member_Creator, user.Projects[uts.userData.Username].Role)
	uts.Equal(portal.Member_Active, user.Projects[uts.userData.Username].State)
	uts.Equal(uts.userData.Institution, user.Institution)
	uts.Equal(uts.userData.Category, user.Category)
	uts.Equal(uts.userData.Email, user.Email)
	uts.Equal(uts.userData.Country, user.Country)
	uts.Equal(uts.userData.Usstate, user.Usstate)
}

func (uts *UserTestSuite) TestUserLogin() {

	// Confirm the user can login
	ctx, err := LoginContext(
		uts.userData.Username,
		uts.userData.Password,
		uts.Assertions,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("got login context for %s: +%v", uts.userData.Username, ctx)
}

func (uts *UserTestSuite) TestDeleteUser() {

	//user already created and activated
	err := DeleteUser(
		uts.opsCtx,
		uts.userData.Username,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("Tryed to delete user %s", uts.userData.Username)

	user, err := GetUser(uts.opsCtx,
		uts.userData.Username,
	)
	uts.NotNil(err, "User should be deleted and return an error")
	uts.Nil(user, "User should be nil after deletion")
}

func (uts *UserTestSuite) TestLogout() {

	err := Logout(uts.userData.Username)
	uts.Nil(err,ErrorStr(err))// Ensure the error is Nil

	//log the obtained login context
	uts.T().Logf("got login context for %s: +%v", uts.userData.Username, uts.opsCtx)
}

func (uts *UserTestSuite) TestActivateUser() {
	
	//user is creadted but not activated nefore
	err := ActivateUser(
		uts.opsCtx,
		uts.userData.Username,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("Trying to activate user %s", uts.userData.Username)

	activeuser, err := GetUser(uts.opsCtx, uts.userData.Username)
	uts.Nil(err, ErrorStr(err))
	uts.Equal(portal.UserState_Active, activeuser.State, "The user should be activated after activation.")

}

func (uts *UserTestSuite) TestAddActiveUser() {

	//adduser already created just check if it worked
	adduser, err := GetUser(uts.opsCtx, uts.userData.Username)
	uts.Nil(err, ErrorStr(err))
	uts.NotNil(adduser, "The user should not be Nil")
	uts.Equal(portal.UserState_Active, adduser.State, "The User should be Active")
}

func (uts *UserTestSuite) TestDeleteActiveUser() {

	//Already have an activated user
	err := DeleteActiveUser(
		uts.userData.Username,
		uts.opsCtx,
		uts.Assertions,
	)
	uts.Nil(err, ErrorStr(err))
	uts.T().Logf("Trying to Delete Active User %s", uts.userData.Username)

	//Checks if the user is Deleted
	deleteuser, err := GetUser(uts.opsCtx, uts.userData.Username)
	uts.NotNil(err, "User should be deleted and return an error")
	uts.Nil(deleteuser, "User should be nil after deletion")
}
