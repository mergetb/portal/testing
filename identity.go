package main

import (
	"context"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

//
// API calls - these will fail on error, so no need to check the
// error.
//

// Unregister - unregisters the given user.
func Unregister(ctx context.Context, uid string) error {

	err := Identity(func(cli portal.IdentityClient) error {
		_, err := cli.Unregister(
			ctx,
			&portal.UnregisterRequest{
				Username: uid,
			},
		)
		return err
	})
	return err
}

// Register - registers the given user. Does not need login context as anyone can register.
func Register(u *UserRegisterData) error {

	err := Identity(
		func(cli portal.IdentityClient) error {
			_, err := cli.Register(
				context.TODO(),
				&portal.RegisterRequest{
					Username:    u.Username,
					Email:       u.Email,
					Password:    u.Password,
					Institution: u.Institution,
					Category:    u.Category,
					Country:     u.Country,
					Usstate:     u.Usstate,
					Name:        u.Name,
				},
			)
			return err
		})

	return err
}

func GetIdentity(ctx context.Context, username string) (*portal.IdentityInfo, error) {

	var id *portal.IdentityInfo
	err := Identity(func(cli portal.IdentityClient) error {
		resp, err := cli.GetIdentity(
			ctx,
			&portal.GetIdentityRequest{
				Username: username,
			},
		)
		if resp != nil {
			id = resp.Identity
		}
		return err
	})

	return id, err
}
